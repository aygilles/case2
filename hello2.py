# importer depuis bs4 et requests :
my_list = []
import lxml
from lxml import html 
import requests
from bs4 import BeautifulSoup
import time

for i in range(1, 15):
	url = 'https://mycfia.com/fr/fournisseurs?' + 'page=' + str(i) + '&' + 'limit=100' 
	response = requests.get(url)
	doc = html.fromstring(response.text)
	urls_fournisseurs_fin = doc.xpath('//*[@id="Suppliers-items"]//*//div[@class="card-supplier card-content mb-3 attr-link"]/@data-location')
	for url_fournisseur_fin in urls_fournisseurs_fin:
		url_complet =  'https://mycfia.com' + str(url_fournisseur_fin)
		
		response = requests.get(url_complet)
		doc_fournisseur =html.fromstring(response.text)
		TITLE = doc_fournisseur.xpath("//h1[contains(@class, 'name mt-5 mb-3')]/text()")[0].strip()
		TYPE = str(doc_fournisseur.xpath("//h2[contains(@class, 'type mb-4')]/text()")[0])
		TOTAL_PRODUITS = str(doc_fournisseur.xpath("//div[contains(@class, 'supplier-view')]/h2[2]/text()")[0])
		DESCRIPTION = doc_fournisseur.xpath("//div[contains(@class, 'detail-bloc')]/p/text()")
		DESCRIPTION_FINALE = ' '.join(DESCRIPTION).strip()
		LOGO = (doc_fournisseur.xpath("//div[contains(@class, 'image-wrapper')]/a/@href"))
		LOGO_URL = ''.join(LOGO).strip()
		ADRESSE = doc_fournisseur.xpath("//div[contains(@class, 'ml-3')]/div[1]/text()")[1].strip()
		WEB = doc_fournisseur.xpath("//div[contains(@class, 'ml-3')]/div[2]/a/text()")
		WEBSITE = ''.join(WEB).strip()
		PHONE = doc_fournisseur.xpath("//div[contains(@class, 'ml-3')]/div[3]/text()")[1].strip()
		MAIL = doc_fournisseur.xpath("//div[contains(@class, 'ml-3')]/div[4]/text()")[1].strip()
		print(type(TITLE), TITLE)
		print(type(TOTAL_PRODUITS), TOTAL_PRODUITS)
		print(type(DESCRIPTION_FINALE), DESCRIPTION_FINALE)
		print(type(LOGO_URL), LOGO_URL)
		print(type(ADRESSE), ADRESSE)
		print(type(WEBSITE), WEBSITE)
		print(type(PHONE), PHONE)
		print(type(MAIL), MAIL)
		print('title: ' + TITLE + ' type: ' + TYPE + ' total produits' + TOTAL_PRODUITS + ' description: ' + DESCRIPTION_FINALE + ' logo url: ' + LOGO_URL + ' adresse' + ADRESSE + ' website: ' + WEBSITE + ' telephone: ' + PHONE + ' mail' + MAIL)
		my_dict = {"URL": url_complet, "TITLE": TITLE, "TYPE": TYPE, "TOTAL PRODUITS": TOTAL_PRODUITS, "DESCRIPTION FINALE": DESCRIPTION_FINALE, "LOGO URL": LOGO_URL, "ADRESSE": ADRESSE, "WEBSITE": WEBSITE, "PHONE": PHONE,"MAIL": MAIL}
		my_list.append(my_dict)
		print(my_list)
	

import csv

fieldnames = ['URL', 'TITLE', 'TYPE', 'TOTAL PRODUITS', 'DESCRIPTION FINALE', 'LOGO URL', 'ADRESSE', 'WEBSITE', 'PHONE', 'MAIL']
with open('csv_case.csv', 'w', newline='') as f:
	writer = csv.DictWriter(f, fieldnames=fieldnames)
	writer.writeheader()
	for elem in my_list:
		writer.writerow(elem)
	